# LaraDo

## Overview

I created this project to showcase my skills in both backend and frontend development. The project is a scalable Todo list application, and I plan to release future versions with additional features.

## Installation

To run the project on your local machine, follow these steps:

1. Create a local database.
2. Run the following commands in the project directory:
```
   npm install
   composer install
   npm run dev
   ```


## Getting Started

Once the installation is complete, you can start using the Todo list application. Simply navigate to the application in your web browser to access the interface.

## Usage

The main functionality of this application is to manage tasks. Users can:

- View existing tasks
- Add new tasks
- Delete tasks

## Configuration

Before running the application, ensure that you update your `.env` file with your database configuration and any other required settings.

## Database Seeding and Migration

The necessary database migrations have already been set up. To apply them, run the following command:

```
php artisan migrate
```