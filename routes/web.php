<?php

use App\Http\Controllers\TaskController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [TaskController::class, 'index'])->name('index');
Route::resource('task',TaskController::class);
// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/', [TaskController::class, 'index'])->name('index');
// Route::get('/hello', [TaskController::class, 'show']);

// Route::get('/insert', [TaskController::class, 'create'])->name('create');
// Route::post('/save', [TaskController::class, 'store'])->name('store');

