<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index(Request $request)
    {
        $filters = $request->only([
            'filter'
        ]);

        $tasks = Task::query();

        if ($filters['filter']) {
            $tasks->where('task', 'like', '%' . $filters['filter'] . '%');
        }

        return inertia('Task/Index', [
            'filters' => $filters,
            'tasks' => $tasks->get()
        ]);
    }

    public function create()
    {
        return inertia('Task/Create');
    }

    public function store(Request $request)
    {
        Task::create($request->validate([
            'task' => 'required|string',
            ])
        );

        return redirect()
            ->route('index')
            ->with('success', 'Task has been created!');
    }

    public function destroy(Task $task)
    {
        $task->delete();

        return redirect()
            ->back()
            ->with('success', 'Task has been deleted!');
    }

    public function edit(Task $task)
    {
        return inertia('Task/Edit', [
            'task' => $task
        ]);
    }

    public function update(Request $request,Task $task)
    {
        $task->update($request->validate([
            'task' => 'required|string',
            ])
        );

        return redirect()
            ->route('index')
            ->with('success', 'Task has been updated!');
    }

    public function show()
    {
        return inertia('Task/Show');
    }
}
